; Copyright 2015 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>
        GET     Hdr:ImageSize.<ImageSize>
        $GetIO
        GET     Hdr:Proc

        GET     Hdr:OSEntries
        GET     Hdr:HALEntries

        GET     hdr.omap543x
        GET     hdr.StaticWS
        GET     hdr.GPIO

        AREA    |Asm$$Code|, CODE, READONLY, PIC

        EXPORT  GPIO_Init
        EXPORT  GPIOx_SetAsOutput
        EXPORT  GPIOx_SetOutput
        EXPORT  GPIOx_SetAndEnableIRQ

GPIO_Init
        ; Don't bother resetting the controllers for now, just make sure no IRQs are enabled
        ADR     a1, L4_GPIO_Table
        MOV     a2, #(GPIO_PIN_MAX >> 5)
        MVN     a3, #0
10
        LDR     a4, [a1], #4
        SUBS    a2, a2, #1
        STR     a3, [a4, #GPIO_IRQSTATUS_CLR_0]
        BNE     %BT10
        MOV     pc, lr

        ; a1 = GPIO # (OMAP)
        ; a2 = initial value (zero or nonzero)
GPIOx_SetAsOutput
        SUBS    a3, a1, #GPIO_PIN_MAX
        MOVGE   pc, lr
        ; OMAP GPIO
        GPIO_PrepareR a3, a4, a1
        GPIO_SetAsOutput a3, a4, a1
        GPIO_SetOutput a2, a3, a4
        MOV     pc, lr

        ; a1 = GPIO # (OMAP)
        ; a2 = value (zero or nonzero)
GPIOx_SetOutput
        SUBS    a3, a1, #GPIO_PIN_MAX
        MOVGE   pc, lr
        ; OMAP GPIO
        GPIO_PrepareR a3, a4, a1
        GPIO_SetOutput a2, a3, a4
        MOV     pc, lr

        ; a1 = GPIO # (OMAP only!)
        ; a2 = IRQ type flags:
        ;      +1 = LEVELDETECT0
        ;      +2 = LEVELDETECT1
        ;      +4 = RISINGDETECT
        ;      +8 = FALLINGDETECT
GPIOx_SetAndEnableIRQ
        GPIO_PrepareR a3, a4, a1
        MRS     ip, CPSR
        ORR     a1, ip, #I32_bit ; interrupts off
        MSR     CPSR_c, a1
        MOV     a2, a2, LSL #28
        MSR     CPSR_f, a2 ; load into NZCV flags (MI EQ CS VS condition codes)
        LDR     a1, [a3, #GPIO_FALLINGDETECT]
        BICPL   a1, a1, a4
        LDR     a2, [a3, #GPIO_RISINGDETECT]
        ORRMI   a1, a1, a4
        STR     a1, [a3, #GPIO_FALLINGDETECT]
        BICNE   a2, a2, a4
        LDR     a1, [a3, #GPIO_LEVELDETECT1]
        ORREQ   a2, a2, a4
        STR     a2, [a3, #GPIO_RISINGDETECT]
        BICCC   a1, a1, a4
        LDR     a2, [a3, #GPIO_LEVELDETECT0]
        ORRCS   a1, a1, a4
        STR     a1, [a3, #GPIO_LEVELDETECT1]
        BICVC   a2, a2, a4
        LDR     a1, [a3, #GPIO_OE]
        ORRVS   a2, a2, a4
        ORR     a1, a1, a4 ; set pin as input
        STR     a2, [a3, #GPIO_LEVELDETECT0]
        STR     a1, [a3, #GPIO_OE]
        STR     a4, [a3, #GPIO_IRQSTATUS_SET_0]
        MSR     CPSR_c, ip ; interrupts restored
        MOV     pc, lr

        END
